# Scaffold

Scaffold is a responsive grid system and UI library developed for PagePress.

# Compiling and Optimizing for Deployment

Scaffold can be compiled in many various ways, but we suggest to use the following method:

- Compiling and minifing LESS to CSS before deploying for faster site load.
- Using dart2js to convert DART to Javascript for deployment.

# Licence

Scaffold, like PagePress, is under the Apache Licence.

A very small amount of the library is loosely refrenced from YUI, under the BSD Licence. Full licencing agreement for YUI can be found in yui.md.
